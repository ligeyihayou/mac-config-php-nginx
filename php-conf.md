# mac config php nginx

#### 描述
**在mac系统中配置nginx php**

#### 1.在mac中默认安装了php环境只需要开启即可
    sudo php-fpm #会遇到的会遇到的问题

    [29-Nov-2018 10:29:34] ERROR: failed to open configuration file '/private/etc/php-fpm.conf': No such file or directory (2)
    [29-Nov-2018 10:29:34] ERROR: failed to load configuration file '/private/etc/php-fpm.conf'

    解决方式：在对应的目录下面找不到 php-fpm.conf 文件(默认存在的是php-fpm.conf.default) 可以将其复制为缺少的文件
        sudo cp  php-fpm.conf.default php-fpm.conf

    [29-Nov-2018 10:33:08] WARNING: Nothing matches the include pattern '/private/etc/php-fpm.d/*.conf' from /private/etc/php-fpm.conf at line 125.
    [29-Nov-2018 10:33:08] ERROR: failed to open error_log (/usr/var/log/php-fpm.log): No such file or directory (2)
    [29-Nov-2018 10:33:08] ERROR: failed to post process the configuration
    [29-Nov-2018 10:33:08] ERROR: FPM initialization failed

    解决方式：在php-fpm.conf(125 line)行出报错,该行内容为：
    include=/private/etc/php-fpm.d/*.conf

    将当前php-fpm.d下面的文件重命名成以(.conf)结尾的文件即可。

    上述错误中 (php-fpm.log)没有存在对应的目录中报错，我这里的修改方式为(再本地创建文件，并赋予755权限)
    ; Default Value: log/php-fpm.log
    error_log = /usr/local/var/log/php-fpm.log

    然后重新启动
        sudo php-fpm