####nginx mac配置

> Homebrew 安装

      /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

      brew install install

      #安装路径
      /usr/local/etc/nginx
      #配置文件
      /usr/local/etc/nginx/nginx.conf
      具体配置信息参考

[nginx.conf具体配置信息参考](https://gitee.com/ligeyihayou/webroot/blob/master/conf.d/nginx.conf)

> nginx 常用命令介绍

    nginx -t  # 检查配置文件

    input: nginx -t

    output:
        nginx: the configuration file /usr/local/etc/nginx/nginx.conf syntax is ok
        nginx: configuration file /usr/local/etc/nginx/nginx.conf test is successful

    nginx -s reload #重新加载配置文件

    nginx -c /**/**/nginx.conf  #启动nginx

    nginx -s stop #关闭nginx服务    